import React, { useEffect, useState } from "react";
import { useKeycloak } from '@react-keycloak/ssr';
import { Dropdown,
         DropdownItem,
         DropdownToggle,
         PageHeaderToolsItem,
         Nav,
         NavItem,
         NavList } from '@patternfly/react-core';
import CaretDownIcon from '@patternfly/react-icons/dist/js/icons/caret-down-icon';
import Link from 'next/link'

function UserMenu(props) {

  const { keycloak, initialized } = useKeycloak()

  const [isOpen, setIsOpen] = useState(false);
  
  const onToggle = () => {
    setIsOpen(!isOpen)
  }
  
  const onSelect = () => {
  };

  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;

  const toggle = <DropdownToggle id="user-menu-dropdown-toggle" onToggle={onToggle} toggleIndicator={CaretDownIcon}>{user_data && user_data.preferred_username}</DropdownToggle>

  // https://zen-swanson-2d3350.netlify.app/components/dropdown/
  // <DropdownItem key="profile"><Link href="/profile"><a className="pf-c-dropdown__menu-item" href="/profile">Profile</a></Link></DropdownItem>,
  // <DropdownItem key="logout"><a className="pf-c-dropdown__menu-item" href={initialized ? keycloak?.createLogoutUrl() : ''}>Logout</a></DropdownItem>
  let dropdownItems = null;
  if ( keycloak != null && keycloak.authenticated ) {
    dropdownItems = [
      <DropdownItem key="profile"><Link href="/profile">Profile</Link></DropdownItem>,
      <DropdownItem key="logout"><Link href={initialized ? keycloak?.createLogoutUrl() : ''}>Logout</Link></DropdownItem>
    ];
  }
  else {
    dropdownItems = [
    ];
  }

  const loginUrl = ( initialized && keycloak != null && keycloak.createLoginUrl != null) ? keycloak.createLoginUrl() : '';

  return (
    <PageHeaderToolsItem>
        { ( initialized && keycloak.authenticated ) ?
            <Dropdown onSelect={onSelect}
                      toggle={toggle}
                      dropdownItems={dropdownItems}
                      isOpen={isOpen}
                      isPlain={true} />
          :
            <Nav variant="horizontal">
              <NavList>
                <NavItem key="login" itemId={5} isActive={false}>
                  <Link href={loginUrl}>
                    <a className="pf-c-nav__link" href={loginUrl}>Login</a>
                  </Link>
                </NavItem>
              </NavList>
            </Nav>
      }
    </PageHeaderToolsItem>
  )
}

export default UserMenu;
