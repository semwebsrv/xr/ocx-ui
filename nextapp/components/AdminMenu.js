import React, { useEffect, useState } from "react";
import { Nav,
         NavItem,
         NavList,
         Toolbar,
         ToolbarContent,
         ToolbarItem } from '@patternfly/react-core';
import Link from 'next/link'
import { useRouter } from 'next/router'



function AdminMenu(props) {

  const router = useRouter()

  return (
    <Toolbar id="AdminToolbar">
      <ToolbarContent>
        <ToolbarItem variant="label">
          Admin Tools:
        </ToolbarItem>
        <ToolbarItem>
          <Nav variant="tertiary" theme="dark">
            <NavList>
              <NavItem key="pendingAccessRequests" itemId="pendingAccessRequests" isActive={false} href="/admin/pendingAccessRequests">
                <Link href="/admin/pendingAccessRequests">
                  <a className={`pf-c-nav__link ${router.pathname?.startsWith('/admin/pendingAccessRequests') ? ' pf-m-current' : ''}`} href="/admin/pendingAccessRequests">Access Requests</a>
                </Link>
              </NavItem>
            </NavList>
          </Nav>
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )
}

export default AdminMenu;
