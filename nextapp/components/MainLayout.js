import React, { useEffect, useState } from "react";
import { useKeycloak } from '@react-keycloak/ssr';
import { Dropdown,
         DropdownItem,
         DropdownToggle,
         Page,
         PageSection,
         PageHeader,
         PageHeaderTools,
         PageHeaderToolsItem,
         Nav,
         NavItem,
         NavList } from '@patternfly/react-core';
import Link from 'next/link'
import UserMenu from './UserMenu'
import AdminMenu from './AdminMenu'
import axios from 'axios'
import { useRouter } from 'next/router'
import package_info from '../package.json'

function MainLayout({Component, pageProps,cookies,children}) {

  const { keycloak, initialized } = useKeycloak()
  const user_data = initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const is_admin = user_data?.realm_access?.roles?.includes('ADMIN')
  const router = useRouter()

  keycloak.onTokenExpired = () => {
    console.log('token expired', keycloak.token);
    keycloak.updateToken(30).success(() => {
      console.log('successfully get a new token', keycloak.token);
      axios.defaults.headers.common['Authorization'] = 'Bearer '+keycloak.token;
    }).error(() => {
      console.log("Problem refreshing token");
    });
  }

  if ( user_data != null ) {
    // console.log("Setting auth to %s",keycloak.token);
    axios.defaults.headers.common['Authorization'] = 'Bearer '+keycloak.token;
  }

  // Inspired by https://github.com/patternfly/patternfly-react-demo-app/blob/d1c9a0c896d52a6d1564e066f2e9f7e2da8bd0ba/src/App.js
  const HeaderTools = (
    <PageHeaderTools>
      <PageHeaderToolsItem>
        <Nav variant="horizontal">
          <NavList>
            { is_admin && ( 
              <NavItem key="admin" itemId={0} isActive={router.pathname?.startsWith('/admin')}>
                <Link href="/admin">
                  <a className={`pf-c-nav__link ${router.pathname?.startsWith('/admin') ? ' pf-m-current' : ''}`} href="/admin">Admin</a>
                </Link>
              </NavItem>) }
            <NavItem key="content" itemId={1} isActive={router.pathname?.startsWith('/content')}>
              <Link href="/content"><a className="pf-c-nav__link" href="/content">Content</a></Link>
            </NavItem>
          </NavList>
        </Nav>
      </PageHeaderToolsItem>
      <UserMenu/>
    </PageHeaderTools>
  );

  const Header = (
    <PageHeader showNavToggle
                logo={`OCX ${package_info.version}`}
                headerTools={HeaderTools}
                />
  );

  return (
    <Page mainContainerId="holoActiveComponent" header={Header} >
      <PageSection isFilled={true} hasOverflowScroll={true}>
        {children}
      </PageSection>
      <PageSection>
        <div style={{"textAlign": "right"}}>
          RebellionContentExchange {package_info.version} ({process.env.NEXT_PUBLIC_ENV_LABEL})
        </div>
      </PageSection>
    </Page>
  )
}

export default MainLayout;
