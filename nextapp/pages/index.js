import React, { useEffect, useState } from "react";
import axios from 'axios'
import { Table, TableHeader, TableBody } from '@patternfly/react-table';
import { useRouter } from 'next/router'

export default function Home() {

  const router = useRouter()

  useEffect( () => {
    router.push("/content");
  })

  return (
    <div>
    </div>
  )
}
