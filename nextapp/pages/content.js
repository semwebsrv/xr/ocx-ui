import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import {
  Button,
  Form,
  FormGroup,
  InputGroup,
  Pagination,
  TextInput,
} from '@patternfly/react-core';


import Link from 'next/link'


export default function Content(props) {

  const [qr, setQr] = useState(props.initialMultiplexes);
  const [pageNumber, setPageNumber] = useState(1);
  const [queryString, setQueryString] = useState('');
  const [textInput, setTextInput] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const query = queryString != null ? '%'+queryString+'%' : '%'
      const offset = ( pageNumber - 1 ) * 10
      const result = await axios('https://uat.semweb.co/dabdata/public/services', { params: {q:query, max:10, offset:offset, sort:'name' } } );
      setQr(result.data);
    };
 
    fetchData();
  }, [queryString, pageNumber]);

  const onSetPage = (event, n) => {
    console.log("set page %o",n);
    setPageNumber(n)
  }

  const onPerPageSelect = (n) => {
  }

  const handleQuery = (event) => {
    setQueryString(textInput);
    setPageNumber(1);
    event.preventDefault();
  }

  const handleTextChange = (n) => {
    setTextInput(n);
  }

  return (
    <>
      <Form onSubmit={handleQuery} isHorizontal>
        <FormGroup label="Content:" fieldId="content-title">
          <InputGroup>
            <TextInput id="qry"
                       isRequired type="text"
                       name="simple-form-name-01"
                       aria-describedby="simple-form-name-01-helper"
                       onChange={handleTextChange}
                       value={textInput} />
            <Button type="submit" variant="primary">Search</Button>
          </InputGroup>
        </FormGroup>
        
      </Form>
 
      <Pagination
        itemCount={qr?.totalCount}
        perPage={10}
        page={pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />

      <TableComposable aria-label="Content List">
        <Thead>
          <Tr>
            <Th>Title</Th>
          </Tr>
        </Thead>
        <Tbody>
          {qr.resultList && qr.resultList.map((row, rowIndex) => (
            <Tr key={'ocx_content_item'+row.id}>
              <Td><Link href={"/services/"+row.id}>{row.title}</Link></Td>
            </Tr>
          ))}
        </Tbody>
      </TableComposable>

    </>
  )
}

export async function getServerSideProps(context) {
  const res = await fetch('https://uat.semweb.co/dabdata/public/services?q=%25&max=10&offset=0')
  const data = await res.json()
  return { props: { initialMultiplexes: data } }
}

