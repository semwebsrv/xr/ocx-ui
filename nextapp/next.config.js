const withTM = require('next-transpile-modules')([
  '@patternfly/react-core',
  '@patternfly/react-styles',
  '@patternfly/react-table'
])

const path = require('path')

module.exports = withTM({
  webpack5: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'react-styles')],
  },
})
